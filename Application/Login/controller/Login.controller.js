var db;
sap.ui.define(['sap/ui/core/mvc/Controller', 'sap/ui/unified/DateRange'],
    function (Controller) {
        "use strict";
        return Controller.extend("SapUI5Tutorial.Application.Login.controller.Login", {
            onInit: function () {

                db = openDatabase('udb', '1.0', 'Test DB', 2 * 1024 * 1024);
            },
            yapGiris: function (oEvent) {
                var that = this;
                var sifre = this.getView().byId("inputSifre").getValue();
                var mail = this.getView().byId("inputEmail").getValue();
                if (sifre.trim() != "" && mail.trim() != "") {
                    LoginService.loginSelect(mail, sifre).then(function (results) {
                        var i;
                        for (i = 0; i < results.rows.length; i++) {
                            var row = results.rows.item(i);
                            var tsifre = row.sifre;
                            var tkullanici = row.kullanici;
                            var tunvan = row.gorev;
                        }
                        if (results.rows.length != 0) {
                            if (mail == tkullanici && tsifre == sifre) {
                                localStorage.setItem("kullanici", mail);
                                localStorage.setItem("unvan", tunvan);
                                that.yonlendir(oEvent);

                            }
                        }
                        else { alert("kullanıcı adı veya şifre hatalı"); }
                    }).catch(function (cevap) { console.log(cevap) })
                }
                else {
                    alert("alanlar boş bırakılmaz");
                }

            },
            yonlendir: function (oEvent) {
                var d = new Date();
                this.getView().byId("inputSifre").setValue("");
                this.getView().byId("inputEmail").setValue("");
                localStorage.setItem("girisSaati", d.getHours());
                localStorage.setItem("girisDakika", d.getMinutes());
                var oItem = oEvent.getSource();
                var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                oRouter.navTo("Personel");
                window.location.reload(false);

            }



        });
    });

