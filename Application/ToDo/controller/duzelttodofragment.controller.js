/*var base, dtkid, dDurum, dkimden,b;
var d = new Date();*/
sap.ui.define([
    'sap/ui/core/mvc/Controller',
    'jquery.sap.global',
    'sap/m/MessageToast',
    'sap/ui/core/Fragment',
    'sap/ui/model/Filter',
    'sap/ui/model/json/JSONModel',
    'sap/m/Dialog',
    'sap/m/Text',
    'sap/m/Button',
    'sap/ui/unified/Menu',
    'sap/ui/unified/MenuItem',
    'sap/m/Token'
],
    function (Controller, jQuery, MessageToast, Fragment, Filter, JSONModel, Dialog, Text, Button, Menu, MenuItem, Token) {
        "use strict";
        return Controller.extend("SapUI5Tutorial.Application.ToDo.controller.duzelttodofragment",{
            beforeOpen: function () {
                
               createComponent.projeSearch("multiInputProje","multiInputProjeBox");
               createComponent.personelSearch("multiInputLogin","multiInputLoginBox");
               createComponent.personelSearch("multiInputBilgi","multiInputBilgiBox");
               var oMultiInputBilgi = sap.ui.getCore().byId('multiInputBilgi');
               var oMultiInputProje = sap.ui.getCore().byId('multiInputProje');
               var oMultiInputKime = sap.ui.getCore().byId('multiInputLogin');
               var pad=localStorage.getItem("PAD");
               oMultiInputProje.setTokens([
                new Token({ text: pad, key: pad })
            ]);
            var tkid=localStorage.getItem("TKID");
            TodoService.rolTabloSelect(tkid, "bilgi").then(function (results) {
                for (var i = 0; i < results.rows.length; i++) {
                    var row = results.rows.item(i);
                    oMultiInputBilgi.addToken(new Token({ text: row.token, key: row.token }));
                }

            })
            TodoService.rolTabloSelect(tkid, "kime").then(function (results) {
                for (var i = 0; i < results.rows.length; i++) {
                    var row = results.rows.item(i);
                    oMultiInputKime.addToken(new Token({ text: row.token, key: row.token }));
                }

            })
        
              
            },
           dcloseDenemeDialog: function (oEvent) {
              console.log(oEvent);
                var closeDenemeDialog = sap.ui.getCore().byId("dzlttodoDialog");
                closeDenemeDialog.destroy();  
            },  
            dKaydetTodo: function () {
                var that=this;
                var dtkid=localStorage.getItem("TKID");
                TodoService.todoDelete(dtkid).then(function (results) {
                    TodoService.rolDelete(dtkid).then(function (results) {
                        var pad = sap.ui.getCore().byId('multiInputProje').getTokens()[0].getText();
                        var konu = sap.ui.getCore().byId('inputKonu').getValue();
                        var durum = sap.ui.getCore().byId('durumBox').getValue();
                        var oncelik = sap.ui.getCore().byId('oncelikBox').getValue();
                        TodoService.todoInsert(dtkid, pad, konu, oncelik, durum).then(function (results) {
                            TodoService.rolInsert(dtkid, "kimden", dkimden).then(function (results) {
                                var a;
                                var kimeTokens = sap.ui.getCore().byId('multiInputLogin').getTokens();
                                kimeTokens.forEach(function (item) {
                                    a = item.getText();
                                    TodoService.rolInsert(dtkid, "kime", a).then(function (results) {

                                    }).catch(function (cevap) {
                                        console.log(cevap)
                                    });
                                })
                                var bilgiTokens = sap.ui.getCore().byId('multiInputBilgi').getTokens();
                                bilgiTokens.forEach(function (item) {
                                    var b = item.getText();
                                    TodoService.rolInsert(dtkid, "bilgi", b).then(function (results) {
                                        sap.ui.getCore().byId('multiInputBilgi').removeAllTokens();
                                        sap.ui.getCore().byId('multiInputLogin').removeAllTokens();
                                        var closeDenemeDialog = sap.ui.getCore().byId("dzlttodoDialog");
                                        closeDenemeDialog.close();
                                        closeDenemeDialog.destroy();
                                        that.createTable();
                                        
                                    }).catch(function (cevap) { console.log(cevap) });

                                })
                            }).catch(function (cevap) {
                                console.log(cevap)
                            });

                        }).catch(function (cevap) {
                            console.log(cevap)
                        });

                    })
                })

            },
            createTable: function () {
                var tnames = [];
                var gelenTkid = [];
                var kimden, oncelik, durum, pad, konu;
                var kime = localStorage.getItem("kullanici");
                TodoService.rolTabloIdSelect("kime", kime).then(function (results) {
                    for (var i = 0; i < results.rows.length; i++) {
                        var row = results.rows.item(i);
                        gelenTkid[i] = row.tkid;
                    }

                    gelenTkid.forEach(function (item) {
                        var kimeName = "";
                        TodoService.rolTabloSelect(item, "kime").then(function (results) {
                            for (var j = 0; j < results.rows.length; j++) {
                                var row = results.rows.item(j);
                                var a = row.token;
                                kimeName = kimeName + " " + a;

                            }
                        }).catch(function (cevap) {
                            console.log(cevap);
                        })
                        TodoService.rolTabloSelect(item, "kimden").then(function (results) {
                            for (var j = 0; j < results.rows.length; j++) {
                                var row = results.rows.item(j);
                                kimden = row.token;
                            }
                        }).catch(function (cevap) {
                            console.log(cevap);
                        })
                        TodoService.todoIdSelect(item).then(function (results) {
                            for (var j = 0; j < results.rows.length; j++) {
                                var row = results.rows.item(j);
                                oncelik = row.oncelik;
                                durum = row.durum;
                                konu = row.konu;
                                pad = row.pad;
                                var obj = { TKID: item, PAD: pad, KONU: konu, SORUMLU: kimeName, OKISI: kimden, DURUM: durum, ONCELIK: oncelik };
                                tnames.push(obj);

                            }
                            oModel.setProperty("/tablename", tnames);
                        }).catch(function (cevap) {
                            console.log(cevap);
                        })
                    })
                }).catch(function (cevap) {
                    console.log(cevap);
                })
            },
           
        });
    });
