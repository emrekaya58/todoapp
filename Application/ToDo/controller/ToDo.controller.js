var base, dtkid, dDurum, dkimden;

var d = new Date();
sap.ui.define([
    'sap/ui/core/mvc/Controller',
    'jquery.sap.global',
    'sap/m/MessageToast',
    'sap/ui/core/Fragment',
    'sap/ui/model/Filter',
    'sap/ui/model/json/JSONModel',
    'sap/m/Dialog',
    'sap/m/Text',
    'sap/m/Button',
    'sap/ui/unified/Menu',
    'sap/ui/unified/MenuItem',
    'sap/m/Token'
],
    function (Controller, jQuery, MessageToast, Fragment, Filter, JSONModel, Dialog, Text, Button, Menu, MenuItem, Token) {
        "use strict";
        return Controller.extend("SapUI5Tutorial.Application.ToDo.controller.ToDo", {
            onInit: function () {
                base = this;
                this.localStorageKontrol();
                var olusturan = localStorage.getItem("kullanici");
                if (olusturan == "1") {
                    this.gotoLogin();
                }
                db = openDatabase('udb', '1.0', 'Test DB', 2 * 1024 * 1024);
                this.todoId();
                this.createTable();
            },
            backPersonel: function (oEvent) {
                this.localStorageKontrol();
                    var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                    oRouter.navTo("Personel");
              
            },
            localStorageKontrol() {
                var saat = parseInt(localStorage.getItem("girisSaati"));
                var dakika = parseInt(localStorage.getItem("girisDakika"));
                var cikisSaat = d.getHours();
                var cikisDakika = d.getMinutes();
                if (localStorage.valueOf().girisDakika == undefined || localStorage.valueOf().girisSaati == undefined||(cikisSaat >= (saat + 1) && cikisDakika >= dakika)) {
                    this.gotoLogin();
                }
            },
            gotoLogin: function () {

                localStorage.setItem("kullanici", "1");
                localStorage.setItem("unvan", "1");

                var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                oRouter.navTo("Login");
                window.location.reload(false);

            },
            todoId: function () {
                TodoService.todoSelect().then(function (results) {
                    var olusturan = localStorage.getItem("kullanici");
                    if (results.rows.length == 0) localStorage.setItem("TK", 1);
                }).catch(function (cevap) { console.log(cevap) });
            },
       
            ekleTK: function (oEvent) {
                this.localStorageKontrol();
                    var olusturan = localStorage.getItem("kullanici");
                   
                        var dialogController = sap.ui.controller("SapUI5Tutorial.Application.ToDo.controller.todofragment");
                        this._oDialog = sap.ui.xmlfragment("SapUI5Tutorial.Application.ToDo.fragment.todo", dialogController);
                        this._oDialog.setModel(this.getView().getModel());
                 
                    sap.ui.getCore().byId('InputKimden').setValue(olusturan);

                    this._oDialog.open();
               
            },
            createTable: function () {
                var tnames = [];
                var gelenTkid = [];
                var kimden, oncelik, durum, pad, konu;
                var kime = localStorage.getItem("kullanici");
                TodoService.rolTabloIdSelect("kime", kime).then(function (results) {
                    for (var i = 0; i < results.rows.length; i++) {
                        var row = results.rows.item(i);
                        gelenTkid[i] = row.tkid;
                    }

                    gelenTkid.forEach(function (item) {
                        var kimeName = "";
                        TodoService.rolTabloSelect(item, "kime").then(function (results) {
                            for (var j = 0; j < results.rows.length; j++) {
                                var row = results.rows.item(j);
                                var a = row.token;
                                kimeName = kimeName + " " + a;

                            }
                        }).catch(function (cevap) {
                            console.log(cevap);
                        })
                        TodoService.rolTabloSelect(item, "kimden").then(function (results) {
                            for (var j = 0; j < results.rows.length; j++) {
                                var row = results.rows.item(j);
                                kimden = row.token;
                            }
                        }).catch(function (cevap) {
                            console.log(cevap);
                        })
                        TodoService.todoIdSelect(item).then(function (results) {
                            for (var j = 0; j < results.rows.length; j++) {
                                var row = results.rows.item(j);
                                oncelik = row.oncelik;
                                durum = row.durum;
                                konu = row.konu;
                                pad = row.pad;
                                var obj = { TKID: item, PAD: pad, KONU: konu, SORUMLU: kimeName, OKISI: kimden, DURUM: durum, ONCELIK: oncelik };
                                tnames.push(obj);

                            }
                            oModel.setProperty("/tablename", tnames);
                        }).catch(function (cevap) {
                            console.log(cevap);
                        })
                    })
                }).catch(function (cevap) {
                    console.log(cevap);
                })
            },
            getMenuParametre: function (oEvent, oRowContext) {
                dDurum = oEvent.getParameter("item").getText();
                var a = oRowContext.sPath;
                var aIndex = a.lastIndexOf("/") + 1;
                var pathName = a.substring(0, (aIndex - 1));
                var satirIndex = a.substring(aIndex);
                dtkid = oRowContext.oModel.getProperty(pathName)[satirIndex].TKID;
            },
            ondeneme: function (oEvent) {
                this.localStorageKontrol();
             
                    if (sap.ui.Device.support.touch) {
                        return;
                    }
                    if (oEvent.getParameter("columnId") == this.getView().createId("TKID") && oEvent.getParameter("columnId") != this.getView().createId("TKID")) {
                        return;
                    }
                    oEvent.preventDefault();
                    var oRowContext = oEvent.getParameter("rowBindingContext");
                    if (!this._oIdContextMenu) {
                        this._oIdContextMenu = new Menu();
                        this.getView().addDependent(this._oIdContextMenu);
                    }

                    this._oIdContextMenu.destroyItems();

                    this._oIdContextMenu.addItem(new MenuItem({
                        text: "Durum",
                        /* select: function () {
                           var proName = oEvent.getParameter("item").getText();
                            console.log(proName);
                         },*/
                        submenu: new Menu({
                            items: [
                                new MenuItem({
                                    text: "yeni",
                                    select: function () {
                                        base.getMenuParametre(oEvent, oRowContext);
                                        TodoService.todoTabloSetDurum(dtkid, dDurum);
                                        base.createTable();

                                    }
                                }),
                                new MenuItem({
                                    text: "planlaniyor",
                                    select: function () {

                                        base.getMenuParametre(oEvent, oRowContext);
                                        TodoService.todoTabloSetDurum(dtkid, dDurum);
                                        base.createTable();


                                    }
                                }),
                                new MenuItem({
                                    text: "tamamlandi",
                                    select: function () {
                                        base.getMenuParametre(oEvent, oRowContext);
                                        TodoService.todoTabloSetDurum(dtkid, dDurum);
                                        base.createTable();

                                    }
                                })

                            ]
                        })
                    }));
                    this._oIdContextMenu.addItem(new MenuItem({
                        text: "Oncelik",
                        // select: function () {
                        //     var proName = oRowContext.getProperty("proName")
                        //     console.log(proName);
                        // },
                        submenu: new Menu({
                            items: [
                                new MenuItem({
                                    text: "düşük",
                                    select: function () {
                                        base.getMenuParametre(oEvent, oRowContext);
                                        TodoService.todoTabloSetOncelik(dtkid, dDurum);
                                        base.createTable();

                                    }
                                }),
                                new MenuItem({
                                    text: "orta",
                                    select: function () {
                                        base.getMenuParametre(oEvent, oRowContext);
                                        TodoService.todoTabloSetOncelik(dtkid, dDurum);
                                        base.createTable();

                                    }
                                }),
                                new MenuItem({
                                    text: "yüksek",
                                    select: function () {
                                        base.getMenuParametre(oEvent, oRowContext);
                                        TodoService.todoTabloSetOncelik(dtkid, dDurum);
                                        base.createTable();
                                    }
                                }),
                                new MenuItem({
                                    text: "çok yüksek",
                                    select: function () {
                                        base.getMenuParametre(oEvent, oRowContext);
                                        TodoService.todoTabloSetOncelik(dtkid, dDurum);
                                        base.createTable();
                                    }
                                })

                            ],
                        })
                    }));
                    this._oIdContextMenu.addItem(new MenuItem({
                        text: "Düzelt",
                        select: function () {
                         base.loadDialogFragment(oRowContext);
                       
                        }  
                    }));
                    var oCellDomRef = oEvent.getParameter("cellDomRef");
                    var eDock = sap.ui.core.Popup.Dock;
                    this._oIdContextMenu.open(false, oCellDomRef, eDock.BeginTop, eDock.BeginBottom, oCellDomRef, "none none");
               
            },
            loadDialogFragment: function (oRowContext) {
                
                    var dialogController = sap.ui.controller("SapUI5Tutorial.Application.ToDo.controller.duzelttodofragment");
                    this._oCreateDialog = sap.ui.xmlfragment("SapUI5Tutorial.Application.ToDo.fragment.duzeltTodo", dialogController);
                    this.getView().addDependent(this._oCreateDialog);
                   
               
               var a = oRowContext.sPath;
               var aIndex = a.lastIndexOf("/") + 1;
               var pathName = a.substring(0, (aIndex - 1));
               var satirIndex = a.substring(aIndex);
               var tkid = oRowContext.oModel.getProperty(pathName)[satirIndex].TKID;
               localStorage.setItem("TKID",tkid);
               var pad = oRowContext.oModel.getProperty(pathName)[satirIndex].PAD;
               localStorage.setItem("PAD",pad);
               var konu = oRowContext.oModel.getProperty(pathName)[satirIndex].KONU;
               dkimden = oRowContext.oModel.getProperty(pathName)[satirIndex].OKISI;
               var durum = oRowContext.oModel.getProperty(pathName)[satirIndex].DURUM;
               var oncelik = oRowContext.oModel.getProperty(pathName)[satirIndex].ONCELIK;
               sap.ui.getCore().byId('InputKimden').setValue(dkimden);
               sap.ui.getCore().byId('inputKonu').setValue(konu);
               sap.ui.getCore().byId('durumBox').setValue(durum);
               sap.ui.getCore().byId('oncelikBox').setValue(oncelik);
                this._oCreateDialog.open();
           }
           
        });
    });
