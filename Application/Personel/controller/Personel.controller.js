var dkullanici, pad = [];
var d = new Date();
sap.ui.define([
    'jquery.sap.global',
    'sap/ui/core/mvc/Controller',
    'sap/ui/model/json/JSONModel',
    'sap/m/Dialog',
    'sap/m/Button',
    'sap/m/Text',
    'sap/m/Input',
    'sap/ui/layout/HorizontalLayout',
    'sap/ui/layout/VerticalLayout'

], function (jQuery, Controller, JSONModel, Dialog, Button, Text, Input, HorizontalLayout, VerticalLayout) {
    "use strict";

    var CController = Controller.extend("SapUI5Tutorial.Application.Personel.controller.Personel", {
        onInit: function () {
            this.localStorageKontrol();
            var olusturan = localStorage.getItem("kullanici");
            if (olusturan == "1") {
                this.gotoLogin();
            }
            var girisYapan = localStorage.getItem("unvan");
            if (girisYapan == "Leader") {
                this.getView().byId("inputPersonelBtn").setVisible(true);
                this.getView().byId("inputProjeBtn").setVisible(true);

            } else if (girisYapan == "Consultant") {
                this.getView().byId("inputPersonelBtn").setVisible(false);
                this.getView().byId("inputProjeBtn").setVisible(false);
            }

            db = openDatabase('udb', '1.0', 'Test DB', 2 * 1024 * 1024);
            this.listeleKullanici();
            this.projeIdKontorol();

        
        },
        gotoLogin: function () {
            localStorage.setItem("kullanici", "1");
            localStorage.setItem("unvan", "1");
            var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            oRouter.navTo("Login");
            window.location.reload(false);
        },
        localStorageKontrol() {
            var saat = parseInt(localStorage.getItem("girisSaati"));
            var dakika = parseInt(localStorage.getItem("girisDakika"));
            var cikisSaat = d.getHours();
            var cikisDakika = d.getMinutes();
            if (localStorage.valueOf().girisDakika == undefined || localStorage.valueOf().girisSaati == undefined||(cikisSaat >= (saat + 1) && cikisDakika >= dakika)) {
                this.gotoLogin();
            }
        },
        quit: function (oEvent) {/*ÇIKIŞ YAP fonksiyonu */
            localStorage.setItem("unvan", "1");
            localStorage.setItem("kullanici", "1");
            var oItem = oEvent.getSource();
            var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            oRouter.navTo("Login");
            window.location.reload(false);

        },

        /* Personel Ekle Fonksiyonları */
        personelEkrani: function () {
            this.localStorageKontrol();
            
           
                if (this.getView().byId("SplitAppDemo").getVisible() == false)
                    this.getView().byId("SplitAppDemo").setVisible(true);
                else this.getView().byId("SplitAppDemo").setVisible(false);
           
        },
        listeleKullanici: function () {
            LoginService.loginSelectListe().then(function (results) {
                var i;
                var names = [];
                for (i = 0; i < results.rows.length; i++) {
                    var row = results.rows.item(i);
                    var obj = { title: row.kullanici, desc: row.gorev, sifre: row.sifre };
                    names.push(obj);
                }
                oModel.setProperty("/names", names);

            }).catch(function (cevap) { console.log(cevap) })
        },
        kaydetPersonel: function (oEvent) {

            var that = this;
            this.localStorageKontrol();
           
                var ekleKullanici = this.getView().byId("inputPersonelEmail").getValue();
                var ekleSifre = this.getView().byId("inputPersonelSifre").getValue();
                var ekleUnvan = this.getView().byId("unvanBox").getValue();
                if (ekleKullanici.trim() != "" && ekleSifre.trim() != "" && ekleUnvan.trim() != "") {
                    LoginService.loginKullaniciKontrol(ekleKullanici).then(function (results) {
                        if (results.rows.length == 0) {
                            LoginService.loginKullaniciInsert(ekleKullanici, ekleSifre, ekleUnvan).then(function (cevap) {
                                console.log("insert");
                                that.refresh();
                            }).catch(function (cevap) { console.log(cevap) })
                        } else {
                            alert("Böyle bir Kullanıcı Mevcut...!");
                        }
                    }).catch(function (cevap) { console.log(cevap) })
                }
                else { alert("Boş alan Bırakmayınız...!"); }
           
        },
        refresh: function () {
            this.getView().byId("inputPersonelEmail").setValue("");
            this.getView().byId("inputPersonelSifre").setValue("");
            this.getView().byId("unvanBox").setValue("");

            this.listeleKullanici();

        },
        sec: function (oEvent) {
            this.localStorageKontrol();
        
                var oSelectedItem = oEvent.getParameter('listItem');
                var sPath = oSelectedItem.getBindingContext().getPath();
                var iSelectedItemIndex = parseInt(sPath.substring(sPath.lastIndexOf('/') + 1));
                dkullanici = oModel.getProperty("/names")[iSelectedItemIndex].title;
                var dsifre = oModel.getProperty("/names")[iSelectedItemIndex].sifre;
                var dunvan = oModel.getProperty("/names")[iSelectedItemIndex].desc;
                this.getView().byId("inputPersonelEmail").setValue(dkullanici);
                this.getView().byId("inputPersonelSifre").setValue(dsifre);
                this.getView().byId("unvanBox").setValue(dunvan);
            
        },
        silPersonel: function (oEvent) {
            this.localStorageKontrol();
            var that = this;
           
                var silKullanici = this.getView().byId("inputPersonelEmail").getValue();
                LoginService.loginKullaniciDelete(silKullanici).then(function (cevap) {
                    console.log("delete");
                    that.refresh();
                }).catch(function (cevap) {
                    console.log(cevap);
                })

          
        },
        duzeltPersonel: function (oEvent) {
            var that = this;
            this.localStorageKontrol();
         
                var ekleKullanici = this.getView().byId("inputPersonelEmail").getValue();
                var ekleSifre = this.getView().byId("inputPersonelSifre").getValue();
                var ekleUnvan = this.getView().byId("unvanBox").getValue();
                if (ekleKullanici.trim() != "" && ekleSifre.trim() != "" && ekleUnvan.trim() != "") {

                    LoginService.loginKullaniciUpdate(ekleKullanici, ekleSifre, ekleUnvan, dkullanici).then(function (cevap) {
                        console.log("delete");
                        that.refresh();
                    }).catch(function (cevap) {
                        console.log(cevap);
                    })


                    that.refresh();
                }
                else { alert("Boş alan Bırakmayınız...!"); }
            
        },
        gitToDo: function (oEvent) {
            this.localStorageKontrol();
          
                var oItem = oEvent.getSource();
                var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                oRouter.navTo("ToDo");
                window.location.reload(false);
           

        },
        /* Proje ekleme fonksiyonları*/
        ekleProje: function () {
            this.localStorageKontrol();
          
                if (!this._oDialog) {
                    this._oDialog = sap.ui.xmlfragment("SapUI5Tutorial.Application.Personel.fragment.proje", this);
                    this._oDialog.setModel(this.getView().getModel());
                }

                this._oDialog.open();
            
        },
        Close: function (oEvent) {
            
            this._oDialog.close();
            window.location.reload(false);
        },
        projeIdKontorol: function () {
            var pnames = [];
            ProjeService.projeListele().then(function (results) {
                var i;
                for (i = 0; i < results.rows.length; i++) {
                    var row = results.rows.item(i);
                    pad[i] = row.projeAdi;
                    var obj = { key: i, pid: row.pid };
                    pnames.push(obj);
                }
                if (results.rows.length == 0) localStorage.setItem("PJ", "1");
                oModel.setProperty("/projename", pnames);

            }).catch(function (cevap) { console.log("cevap") })

        },
        projeChange: function (oEvent) {
            var index = sap.ui.getCore().byId('projeBox').getSelectedKey();
            sap.ui.getCore().byId('projeAd').setValue(pad[index]);
        },
        KaydetProje: function () {
            
            var pjid = parseInt(localStorage.getItem("PJ"));
            var pid;
            var pj = "PJ"
            var sifir = "000000";
            var id, idLength;
            id = parseInt(sifir) + pjid;
            idLength = id.toString().length;
            pid = pj + sifir.substring(0, (6 - idLength)) + id.toString();
            var ipad = sap.ui.getCore().byId('projeAd').getValue();

            var sart = true;
            ipad = ipad.toUpperCase();

            for (var i = 0; i < pad.length; i++)
                if (pad[i] == ipad) var sart = false;
            if (sart) {
                pjid++;
                localStorage.setItem("PJ", pjid);
                ProjeService.projeInsert(pid, ipad).then(function (results) {
                    console.log("insert");
                }).catch(function (cevap) { console.log(cevap) })
            }

            this._oDialog.close();
            window.location.reload(false);
        },
        SilProje: function () {
            
            var projeId = sap.ui.getCore().byId('projeBox').getValue();
            ProjeService.projeDelete(projeId).then(function (results) {
                console.log("delete");
            }).catch(function (cevap) { console.log(cevap) })

            this._oDialog.close();
            window.location.reload(false);

        },
        DuzeltProje: function (oEvent) {
          
            var index = sap.ui.getCore().byId('projeBox').getSelectedKey();
            var projeId = sap.ui.getCore().byId('projeBox').getValue();
            var dpAd = sap.ui.getCore().byId('projeAd').getValue();
            dpAd = dpAd.toUpperCase();
            var sart = true;
            for (var i = 0; i < pad.length; i++)if (pad[i] == dpAd){ 
               
                sart = false;
            }
            if (sart) {
                ProjeService.projeUpdate(dpAd, projeId).then(function (results) {
                    console.log("update");
                   TodoService.todoTabloSetProjeAdi(dpAd, pad[index]).then(function (results) {
                        console.log("update");
                        
                    }).catch(function (cevap) { console.log(cevap) })
                    
                }).catch(function (cevap) { console.log(cevap) })

            }

            this._oDialog.close();
          // window.location.reload(false);

        }
    });
    return CController;

});





