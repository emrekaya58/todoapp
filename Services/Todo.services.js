var db;
var TodoService={
    todoSelect:function(){
        return new Promise(function(resolve,reject){
            db.transaction(function(t){
                t.executeSql('SELECT * FROM Todo',[],function(transaction,results){
                 resolve(results);
                },
                function(eror){
                reject(eror);
                });
            })
        });
    },
    todoIdSelect:function(tkid){
        return new Promise(function(resolve,reject){
            db.transaction(function(t){
                t.executeSql('SELECT * FROM Todo WHERE tkid="'+tkid+'"',[],function(transaction,results){
                 resolve(results);
                },
                function(eror){
                reject(eror);
                });
            })
        });
    },
    todoInsert:function(tkid,pad,konu,oncelik,durum){
        return new Promise(function(resolve,reject){
            db.transaction(function(t){
                t.executeSql('INSERT INTO Todo(tkid,pad,konu,oncelik,durum)VALUES(?,?,?,?,?)',[tkid,pad,konu,oncelik,durum],function(results){
                 resolve(results);
                },
                function(eror){
                reject(eror);
                });
            })
        });
    },
    rolInsert:function(tkid,type,token){
        return new Promise(function(resolve,reject){
            db.transaction(function(t){
                t.executeSql('INSERT INTO rtable(tkid,type,token)VALUES(?,?,?)',[tkid,type,token],function(results){
                 resolve(results);
                },
                function(eror){
                reject(eror);
                });
            })
        });
    },
    rolTabloIdSelect:function(type,token){
        return new Promise(function(resolve,reject){
            db.transaction(function(t){
                t.executeSql('SELECT tkid FROM rtable WHERE type="'+type+'" AND token="'+token+'"',[],function(transaction,results){
                 resolve(results);
                },
                function(eror){
                reject(eror);
                });
            })
        });
    },
    rolTabloSelect:function(tkid,type){
        return new Promise(function(resolve,reject){
            db.transaction(function(t){
                t.executeSql('SELECT token FROM rtable WHERE tkid="'+tkid+'" AND type="'+type+'"',[],function(transaction,results){
                 resolve(results);
                },
                function(eror){
                reject(eror);
                });
            })
        });
    },
    todoTabloSetDurum:function(tkid,durum){
        return new Promise(function(resolve,reject){
            db.transaction(function(t){
                t.executeSql('UPDATE Todo set durum=? WHERE tkid=?',[durum,tkid],function(results){
                 resolve(results);
                },
                function(eror){
                reject(eror);
                });
            })
        });

    },
    todoTabloSetOncelik:function(tkid,oncelik){
        return new Promise(function(resolve,reject){
            db.transaction(function(t){
                t.executeSql('UPDATE Todo set oncelik=? WHERE tkid=?',[oncelik,tkid],function(results){
                 resolve(results);
                },
                function(eror){
                reject(eror);
                });
            })
        });

    },
    rolDelete:function(tkid){
        return new Promise(function(resolve,reject){
            db.transaction(function(t){
                t.executeSql('DELETE FROM rtable WHERE tkid=?',[tkid],function(results){
                 resolve(results);
                },
                function(eror){
                reject(eror);
                });
            })
        });
    },
    todoDelete:function(tkid){
        return new Promise(function(resolve,reject){
            db.transaction(function(t){
                t.executeSql('DELETE FROM Todo WHERE tkid=?',[tkid],function(results){
                 resolve(results);
                },
                function(eror){
                reject(eror);
                });
            })
        });
    },
    todoTabloSetProjeAdi:function(dpad,pad){
        return new Promise(function(resolve,reject){
            db.transaction(function(t){
                t.executeSql('UPDATE Todo set pad=? WHERE pad=?',[dpad,pad],function(results){
                 resolve(results);
                },
                function(eror){
                reject(eror);
                });
            })
        });

    },


}