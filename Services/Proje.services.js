var db;
var ProjeService={
   projeListele:function(){
        return new Promise(function(resolve,reject){
            db.transaction(function(t){
                t.executeSql('SELECT * FROM Proje',[],function(transaction,results){
                    resolve(results);
                },
                function(eror){
                    reject(eror);
                })
            })
        });
    },
    projeListeleLike:function(ara){
        return new Promise(function(resolve,reject){
            db.transaction(function(t){
                t.executeSql("SELECT * FROM Proje WHERE projeAdi LIKE '"+ara+"%'",[],function(transaction,results){
                    resolve(results);
                },
                function(eror){
                    reject(eror);
                })
            })
        });
    },
    projeInsert:function(pid,ipad){
        return new Promise(function(resolve,reject){
            db.transaction(function(t){
                t.executeSql('INSERT INTO Proje (pid,projeAdi)VALUES(?,?)',[pid,ipad],function(results){
                    resolve(results);
                },
                function(eror){
                    reject(eror);
                })
            })
        });  
    },
    projeDelete:function(pid){
        return new Promise(function(resolve,reject){
            db.transaction(function(t){
                t.executeSql('DELETE FROM Proje  WHERE pid=?',[pid],function(results){
                    resolve(results);
                },
                function(eror){
                    reject(eror);
                })
            })
        });  
    },
    projeUpdate:function(pad,pid){
        return new Promise(function(resolve,reject){
            db.transaction(function(t){
                t.executeSql('UPDATE  Proje set projeAdi=? WHERE pid=?',[pad,pid],function(results){
                    resolve(results);
                },
                function(eror){
                    reject(eror);
                })
            })
        });  
    }

}