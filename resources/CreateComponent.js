var createComponent={
    projeSearch:function(multiId,vboxid){ 
        var vbox=sap.ui.getCore().byId(vboxid);
        vbox.addItem( new sap.m.MultiInput(multiId,{
           liveChange:function(oEvent){
            var oMulti = oEvent.getParameters().value
            var mInput=sap.ui.getCore().byId(multiId);
            mInput.removeAllSuggestionItems();
               if(oMulti.length>2){
                   var ara=oMulti.toUpperCase();
                   ProjeService.projeListeleLike(ara).then(function (results) {
                    var dizi = Object.assign([],results.rows);
                    dizi.forEach(function(item) {
                        mInput.addSuggestionItem(new sap.ui.core.Item({ key: item.pid, text: item.projeAdi }));           
                      });
                       
                }).catch(function (cevap) { console.log(cevap) });
               }    
        }
        }))
       
 
    },
   personelSearch:function(multiId,vboxid){
    var vbox=sap.ui.getCore().byId(vboxid);
    vbox.addItem( new sap.m.MultiInput(multiId,{
        
       liveChange:function(oEvent){
        var oMulti = oEvent.getParameters().value
        var mInput=sap.ui.getCore().byId(multiId);
        mInput.removeAllSuggestionItems();
           if(oMulti.length>2){
            
            LoginService.loginSelectListeLike(oMulti).then(function (results) {
                var dizi = Object.assign([],results.rows);
                dizi.forEach(function(item) {
                    mInput.addSuggestionItem(new sap.ui.core.Item({ key: item.rowid, text: item.kullanici }));           
                  });


                
                
            }).catch(function (cevap) { console.log(cevap) });
           }    
    }
    }))
   


   }
       


}